"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import smtplib
import socket

from datetime import datetime

from ..sender.smtp import smtp_format_datetime


class UmtpReader(object):
    fqdn = socket.getfqdn()

    STATE_COMMAND = object()
    STATE_DATA = object()

    def __init__(self, f_in, host='localhost', port=0):
        self.__state = self.STATE_COMMAND
        self.__in = f_in
        self.__out = f_out = smtplib.SMTP(host=host, port=0)

        while self.__state == self.STATE_COMMAND:
            line = f_in.readline().rstrip(b'\r\n').decode('ascii', 'replace')
            l = line.split(' ')
            command = l[0].upper()
            method = getattr(self, 'cmd_' + command, None)
            if not method:
                raise Exception('Error: command not recognized')
            method(l[1:])

        if self.__state == self.STATE_DATA:
            while True:
                d = f_in.read(16384)
                if not d:
                    break
                f_out.send(d)

            self.__out.send(b'.\r\n')
            code, repl = self.__out.getreply()
            if code >= 300:
                raise smtplib.SMTPDataError(code, repl)

        self.__out.quit()

    def cmd_UHLO(self, args):
        self.__out.ehlo()

        self.hello = args.pop(0)
        for i in args:
            if not self.__out.has_extn(i):
                raise Exception('Error: lacks feature {}'.format(i))

    def cmd_MAIL(self, args):
        if not args:
            raise Exception('Syntax: MAIL FROM:<address>')

        addr = args[0]
        if not addr.upper().startswith('FROM:<') or not addr.endswith('>'):
            raise Exception('Syntax: MAIL FROM:<address>')
        addr = addr[6:-1]

        code, repl = self.__out.mail(addr, args[1:])
        if code >= 300:
            raise smtplib.SMTPDataError(code, repl)

    def cmd_RCPT(self, args):
        if not args:
            raise Exception('Syntax: RCPT TO:<address>')

        addr = args[0]
        if not addr.upper().startswith('TO:<') or not addr.endswith('>'):
            raise Exception('Syntax: RCPT TO:<address>')
        self.rcptto = addr = addr[4:-1]

        code, repl = self.__out.rcpt(addr, args[1:])
        if code >= 300:
            raise smtplib.SMTPDataError(code, repl)

    def cmd_DATA(self, args):
        if args:
            raise Exception('Syntax: DATA')

        self.__out.putcmd("data")
        code, repl = self.__out.getreply()
        if code != 354:
            raise smtplib.SMTPDataError(code, repl)

        self.__out.send('Received: from {}\r\n'.format(self.hello))
        self.__out.send('\tby {} with UMTP\r\n'.format(self.fqdn))
        self.__out.send('\tfor <{}>; {}\r\n'.format(self.rcptto, smtp_format_datetime(datetime.utcnow())))

        self.__state = self.STATE_DATA

