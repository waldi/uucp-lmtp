"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

from .smtp import SmtpException
from .smtpbase_channel import SmtpBaseChannel

logger = logging.getLogger(__name__)


class LmtpChannel(SmtpBaseChannel):
    greeting_protocol = 'LMTP'
    hello_cmd = 'LHLO'

    def __init__(self, conn, handler):
        super(LmtpChannel, self).__init__(conn)
        self.__handler = handler
        self.__init()

    def __init(self):
        self.__handlers = []

    def reset(self):
        super(LmtpChannel, self).reset()

        for rcpt, handler in self.__handlers:
            handler.abort()
        self.__init()

    def cmd_LHLO(self, args):
        self.reply_hello(args)

    def handle_rcptto(self, rcptto):
        handler = self.__handler(self.mailfrom, rcptto)
        handler.__exception = None
        self.__handlers.append((rcptto, handler))

    def handle_data(self):
        for rcpt, handler in self.__handlers:
            try:
                handler.data()

            except Exception as e:
                logger.exception('exception: DATA from [{}]:'.format(self.addr[0]))
                handler.__exception = e
                handler.abort()

    def handle_data_line(self, line):
        for rcpt, handler in self.__handlers:
            if not handler.__exception:
                try:
                    handler.data_line(line)

                except Exception as e:
                    logger.exception('exception: DATA from [{}]:'.format(self.addr[0]))
                    handler.__exception = e
                    handler.abort()

    def handle_eod(self):
        for rcpt, handler in self.__handlers:
            try:
                if handler.__exception:
                    raise handler.__exception

                try:
                    handler.eod()
                    self.reply(250, '2.0.0', '<{}> Ok'.format(rcpt.addr))

                except SmtpException:
                    raise

                except Exception:
                    logger.exception('exception: END-OF-MESSAGE from [{}]:'.format(self.addr[0]))
                    raise

            except SmtpException as e:
                self.reply_log('END-OF-MESSAGE', e.code, e.encode, '<{}> {}'.format(rcpt.addr, e.msg))

            except Exception:
                self.reply(450, '4.3.0', '<{}> Exception'.format(rcpt.addr))
