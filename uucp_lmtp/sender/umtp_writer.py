"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import socket


class UmtpWriter(object):
    fqdn = socket.getfqdn()

    def __init__(self, f, mailfrom, rcptto):
        self.__f = f
        self.__write('UHLO {} 8BITMIME DSN SIZE\r\n'.format(self.fqdn))
        self.__write_addr('MAIL FROM', mailfrom)
        self.__write_addr('RCPT TO', rcptto)
        self.__write(b'DATA\r\n')

    def __write(self, data):
        if isinstance(data, str):
            data = data.encode('ascii', 'replace')
        self.__f.write(data)

    def __write_addr(self, cmd, info):
        self.__write('{}:<{}>'.format(cmd, info.addr))
        for o in info.options.items():
            self.__write(' {}={}'.format(*o))
        self.__write(b'\r\n')

    def close(self):
        self.__f.close()

    def terminate(self):
        self.__f.terminate()

    def line(self, line):
        if isinstance(line, bytes) and line.startswith(b'.'):
            self.__write(b'.')
        self.__write(line)
        self.__write(b'\r\n')
