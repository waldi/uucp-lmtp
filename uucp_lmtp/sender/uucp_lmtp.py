"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

from .lmtp import LmtpHandlerBase
from .process_wrapper import ProcessWrapper
from .smtp import SmtpErrorLogException
from .umtp_writer import UmtpWriter

logger = logging.getLogger(__name__)


class UucpCommandMap:
    __slots__ = '__map'

    def __init__(self, *args, **kw):
        self.__map = dict(*args, **kw)

    def __getitem__(self, domain):
        rdomain = domain

        while rdomain:
            if rdomain in self.__map:
                return self.__map[rdomain]

            # Search for next period
            i = rdomain.find('.', 1)

            # If no period is found, return global wildcard
            if i == -1:
                if '*' in self.__map:
                    return self.__map['*']
                raise KeyError(domain)

            # Shorten to next period
            rdomain = rdomain[i:]

    def __setitem__(self, domain, command):
        self.__map[domain] = command

    def __delitem__(self, domain):
        del self.__map[domain]


class UucpLmtpHandler(LmtpHandlerBase):
    def __init__(self, mailfrom, rcptto, command):
        super(UucpLmtpHandler, self).__init__(mailfrom, rcptto)
        self.__command = command
        self.__writer = None

    def abort(self):
        if self.__writer:
            self.__writer.terminate()

    def data(self):
        p = ProcessWrapper(('uux', '-r', '-n', '-z', '-apostmaster', '-', self.__command))
        self.__writer = UmtpWriter(p, self.mailfrom, self.rcptto)

    def data_line(self, line):
        self.__writer.line(line)

    def eod(self):
        self.__writer.close()
        logger.info('from=<{}>, to=<{}>, status=delivered, command={}' \
                .format(self.mailfrom.addr, self.rcptto.addr, self.__command))


class UucpLmtpHandlerFactory:
    def __init__(self, commandmap):
        self.__commandmap = commandmap

    def __call__(self, mailfrom, rcptto):
        rcptto_local, rcptto_domain = rcptto.addr.split('@', 1)
        try:
            command = self.__commandmap[rcptto_domain]
        except KeyError:
            raise SmtpErrorLogException(554, '5.1.2', 'No command defined for domain')
        return UucpLmtpHandler(mailfrom, rcptto, self.__commandmap[rcptto_domain])
