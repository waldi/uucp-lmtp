"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import asyncore
import errno
import logging
import socket

logger = logging.getLogger(__name__)


class AsyncServer(asyncore.dispatcher):
    def __init__(self, sockinfo, channel, channel_args):
        super(AsyncServer, self).__init__()

        self.create_socket(*sockinfo[:2])

        try:
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            if sockinfo[0] == socket.AF_INET6:
                self.socket.setsockopt(socket.IPPROTO_IPV6, socket.IPV6_V6ONLY, 1)
            self.bind(sockinfo[4])
            self.listen(5)

            name = socket.getnameinfo(sockinfo[4], socket.NI_NUMERICHOST | socket.NI_NUMERICSERV)
            logger.debug('Listen for incoming connections on %s, port %s', *name)
        except:
            self.close()
            raise

        self.channel, self.channel_args = channel, channel_args

    def handle_accepted(self, conn, addr):
        channel = self.channel(conn, *self.channel_args)


def create_asyncserver(channel, *args, addr=None, port=10024, family=0):
    if addr is None:
        flags = 0
    else:
        if addr == '':
            addr = None
        flags = socket.AI_PASSIVE

    count = 0
    for info in socket.getaddrinfo(addr, port, family, 0, socket.SOL_TCP, flags):
        try:
            AsyncServer(info, channel, args)
            count += 1
        except IOError as e:
            if e.errno == errno.EAFNOSUPPORT:
                continue
            raise
    if not count:
        raise Exception
