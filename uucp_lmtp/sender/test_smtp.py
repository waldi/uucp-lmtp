"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from .smtp import *


class TestSmtpAddr:
    addr = 'test@example.com'
    cls = SmtpAddr

    def test_plain(self):
        a = self.cls(self.addr, ())
        assert a.addr == self.addr
        assert not a.options

    def test_option_unknown(self):
        with pytest.raises(SmtpErrorException):
            self.cls(self.addr, ('TEST', ))


class TestSmtpMailFrom(TestSmtpAddr):
    cls = SmtpMailFrom

    def test_option_BODY_7BIT(self):
        a = self.cls(self.addr, ('BODY=7bIt', ))
        assert a.options['BODY'] == '7BIT'

    def test_option_BODY_8BITMIME(self):
        a = self.cls(self.addr, ('BODY=8bItMiMe', ))
        assert a.options['BODY'] == '8BITMIME'

    @pytest.mark.xfail
    def test_option_BODY_unknown(self):
        with pytest.raises(SmtpErrorException):
            self.cls(self.addr, ('RET=TEST', ))

    def test_option_ENVID(self):
        a = self.cls(self.addr, ('ENVID=1234', ))
        assert a.options['ENVID'] == '1234'

    def test_option_RET_FULL(self):
        a = self.cls(self.addr, ('RET=FuLl', ))
        assert a.options['RET'] == 'FULL'

    def test_option_RET_HDRS(self):
        a = self.cls(self.addr, ('RET=HdRs', ))
        assert a.options['RET'] == 'HDRS'

    @pytest.mark.xfail
    def test_option_RET_unknown(self):
        with pytest.raises(SmtpErrorException):
            self.cls(self.addr, ('RET=TEST', ))


class TestSmtpRcptTo(TestSmtpAddr):
    cls = SmtpRcptTo

    def test_option_NOTIFY_NEVER(self):
        a = self.cls(self.addr, ('NOTIFY=NeVeR', ))
        assert a.options['NOTIFY'] == 'NEVER'

    def test_option_NOTIFY_SUCCESS(self):
        a = self.cls(self.addr, ('NOTIFY=SuCcEsS', ))
        assert a.options['NOTIFY'] == 'SUCCESS'

    def test_option_NOTIFY_FAILURE(self):
        a = self.cls(self.addr, ('NOTIFY=FaIlUrE', ))
        assert a.options['NOTIFY'] == 'FAILURE'

    def test_option_NOTIFY_DELAY(self):
        a = self.cls(self.addr, ('NOTIFY=DeLaY', ))
        assert a.options['NOTIFY'] == 'DELAY'

    def test_option_NOTIFY_combination(self):
        a = self.cls(self.addr, ('NOTIFY=SUCCESS,FAILURE', ))
        assert a.options['NOTIFY'] == 'SUCCESS,FAILURE'

    @pytest.mark.xfail
    def test_option_NOTIFY_combination_invalid(self):
        with pytest.raises(SmtpErrorException):
            self.cls(self.addr, ('NOTIFY=NEVER,FAILURE', ))

    @pytest.mark.xfail
    def test_option_NOTIFY_unknown(self):
        with pytest.raises(SmtpErrorException):
            self.cls(self.addr, ('NOTIFY=TEST', ))


