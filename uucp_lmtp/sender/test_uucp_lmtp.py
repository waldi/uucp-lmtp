"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest

from .uucp_lmtp import *


class TestUucpCommandMap:
    def test(self):
        m = UucpCommandMap()
        with pytest.raises(KeyError):
            m['example.com']

    def test_simple(self):
        m = UucpCommandMap()
        m['example.com'] = 'system'
        assert m['example.com'] == 'system'
        del m['example.com']
        with pytest.raises(KeyError):
            m['example.com']

    def test_subdomain(self):
        m = UucpCommandMap()
        m['.example.com'] = 'system0'
        assert m['test1.example.com'] == 'system0'
        m['test1.example.com'] = 'system1'
        assert m['test1.example.com'] == 'system1'
        assert m['test2.example.com'] == 'system0'
        with pytest.raises(KeyError):
            m['example.com']
            m['example.org']

    def test_subdomain_root(self):
        m = UucpCommandMap()
        m['*'] = 'system0'
        m['.example.com'] = 'system1'
        assert m['example.com'] == 'system0'
        assert m['test.example.com'] == 'system1'
        assert m['example.org'] == 'system0'
