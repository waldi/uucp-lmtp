"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import asynchat
import logging
import socket

from datetime import datetime

from .smtp import *

logger = logging.getLogger(__name__)


class SmtpBaseChannel(asynchat.async_chat):
    fqdn = socket.getfqdn()
    greeting_protocol = 'UNKNOWN'
    hello_cmd = 'UNKNOWN'

    STATE_COMMAND = object()
    STATE_DATA = object()

    def __init__(self, conn):
        super(SmtpBaseChannel, self).__init__(conn)

        self.set_terminator(b'\r\n')
        self.__init()

        self.addr_rfc = 'unknown'
        if self.socket.family == socket.AF_INET:
            self.addr_rfc = self.addr[0]
        elif self.socket.family == socket.AF_INET6:
            self.addr_rfc = 'IPv6:{}'.format(self.addr[0])
        else:
            raise Exception("socket family %s" % (self.addr, ))

        logger.info('connect from [{}]'.format(self.addr[0]))
        self.reply(220, '', '{} {}'.format(self.fqdn, self.greeting_protocol))

    def __init(self):
        self.__state = self.STATE_COMMAND
        self.__data = []
        self.hello = None
        self.mailfrom = None
        self.rcptto = []

    def reset(self):
        self.__init()

    def reply(self, code, encode, *msg):
        if encode:
            encode += ' '
        logger.debug('S: {:d} {}{}'.format(code, encode, ' '.join(msg)))
        for line in msg[:-1]:
            self.push('{:d}-{}{}\r\n'.format(code, encode, line).encode('ascii'))
        self.push('{:d} {}{}\r\n'.format(code, encode, msg[-1]).encode('ascii'))

    def reply_log(self, cmd, code, encode, *msg):
        logger.info('error: {} from [{}]: {:d} {} {}'.format(cmd, self.addr[0], code, encode, ' '.join(msg)))
        self.reply(code, encode, *msg)

    def reply_hello(self, args):
        if len(args) != 1:
            raise SmtpErrorException(501, '', 'Syntax: {} hostname'.format(self.hello_cmd))

        self.hello = args[0]
        self.reply(250, '', self.fqdn,
                'PIPELINING', 'SIZE', 'ENHANCEDSTATUSCODES', '8BITMIME', 'DSN')

    def collect_incoming_data(self, data):
        self.__data.append(data)

    def found_terminator(self):
        data = b''.join(self.__data)
        self.__data = []

        if self.__state is self.STATE_COMMAND:
            try:
                line = data.decode('ascii')
                logger.debug('C: {}'.format(line))

                if not line:
                    raise SmtpErrorException(500, '5.5.2', 'Error: bad syntax')
                l = line.split(' ')
                command = l[0].upper()
                method = getattr(self, 'cmd_' + command, None)
                if not method:
                    raise SmtpErrorException(502, '5.5.2', 'Error: command not recognized')
                method(l[1:])

            except SmtpErrorLogException as e:
                self.reply_log(command, e.code, e.encode, e.msg)

            except SmtpException as e:
                self.reply(e.code, e.encode, e.msg)

            except Exception:
                logger.exception('exception: {} from [{}]:'.format(command, self.addr[0]))
                self.reply(command, 450, '4.3.0', 'Exception')

        elif self.__state is self.STATE_DATA:
            try:
                if data.startswith(b'.'):
                    if len(data) == 1:
                        try:
                            self.handle_eod()
                        finally:
                            self.reset()
                    else:
                        self.handle_data_line(data[1:])
                else:
                    self.handle_data_line(data)

            except Exception:
                logger.exception('exception: DATA from [{}]:'.format(self.addr[0]))
                self.reply(421, '4.3.0', 'Exception')
                self.close_when_done()

    def handle_close(self):
        logger.info('disconnect from [{}]'.format(self.addr[0]))
        self.close()

    def cmd_NOOP(self, args):
        self.reply(250, '2.0.0', 'Ok')

    def cmd_QUIT(self, args):
        self.reply(221, '2.0.0', 'Bye')
        self.close_when_done()

    def cmd_RSET(self, args):
        if args:
            raise SmtpErrorException(501, '5.5.4', 'Syntax: RSET')

        self.reset()
        self.reply(250, '2.0.0', 'Ok')

    def cmd_MAIL(self, args):
        if not self.hello:
            raise SmtpErrorException(503, '5.5.1', 'Error: send {} first'.format(self.hello_cmd))
        if self.mailfrom:
            raise SmtpErrorException(503, '5.5.1', 'Error: nested MAIL command')
        if not args:
            raise SmtpErrorException(501, '5.5.4', 'Syntax: MAIL FROM:<address>')

        addr = args[0]
        if not addr.upper().startswith('FROM:<') or not addr.endswith('>'):
            raise SmtpErrorException(501, '5.5.4', 'Syntax: MAIL FROM:<address>')
        addr = addr[6:-1]

        mailfrom = SmtpMailFrom(addr, args[1:])

        self.handle_mailfrom(mailfrom)
        self.mailfrom = mailfrom
        self.reply(250, '2.1.0', 'Ok')

    def cmd_RCPT(self, args):
        if not self.mailfrom:
            raise SmtpErrorException(503, '5.5.1', 'Error: need MAIL command')
        if not args:
            raise SmtpErrorException(501, '5.5.4', 'Syntax: RCPT TO:<address>')

        addr = args[0]
        if not addr.upper().startswith('TO:<') or not addr.endswith('>'):
            raise SmtpErrorException(501, '5.5.4', 'Syntax: RCPT TO:<address>')
        addr = addr[4:-1]

        rcptto = SmtpRcptTo(addr, args[1:])

        self.handle_rcptto(rcptto)
        self.rcptto.append(rcptto)
        self.reply(250, '2.1.5', 'Ok')

    def cmd_DATA(self, args):
        if not self.rcptto:
            raise SmtpErrorException(503, '5.5.1', 'Error: need RCPT command')
        if args:
            raise SmtpErrorException(501, '5.5.4', 'Syntax: DATA')

        self.handle_data()
        self.handle_data_line('Received: from {} ([{}])'.format(self.hello, self.addr_rfc))
        self.handle_data_line('\tby {} with {};'.format(self.fqdn, self.greeting_protocol))
        self.handle_data_line('\t{}'.format(smtp_format_datetime(datetime.utcnow())))

        self.__state = self.STATE_DATA
        self.reply(354, '', 'End data with <CR><LF>.<CR><LF>')

    def handle_mailfrom(self, mailfrom):
        pass

    def handle_rcptto(self, rcptto):
        pass

    def handle_data(self):
        pass

    def handle_data_line(self, chunk):
        pass

    def handle_eod(self):
        raise NotImplementedError
