"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import errno

from ..support import subprocess
from .smtp import SmtpExitException, SmtpTimeoutException


class ProcessWrapper(object):
    def __init__(self, args):
        null = open('/dev/null')
        self.proc = subprocess.Popen(
                args, bufsize=-1,
                stdin=subprocess.PIPE, stdout=null, stderr=null,
        )

    def __del__(self):
        try:
            p = self.proc
        except AttributeError:
            return
        if p.poll():
            try:
                p.terminate()
            except OSError:
                pass

    def write(self, data):
        if self.proc.poll():
            raise SmtpExitException(self.proc.returncode)
        self.proc.stdin.write(data)

    def close(self):
        self.proc.stdin.close()

        try:
            self.proc.wait(30)
        except subprocess.TimeoutExpired:
            self.terminate()
            raise SmtpTimeoutException

        if self.proc.returncode:
            raise SmtpExitException(self.proc.returncode)

    def terminate(self):
        self.__del__()
