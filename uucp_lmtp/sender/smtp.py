"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os


class SmtpAddr:
    __slots__ = 'addr', 'options'

    def __init__(self, addr, options):
        self.addr = addr
        self.options = {}

        for option in options:
            if '=' in option:
                name, value = option.split('=', 1)
                method = getattr(self, 'option_' + name.upper(), None)
                if method:
                    method(value)
                    continue
            raise SmtpErrorException(555, '5.5.4', 'Unsupported option: {}'.format(option))


class SmtpMailFrom(SmtpAddr):
    __slots__ = ()

    def option_BODY(self, value):
        # XXX: Check
        self.options['BODY'] = value.upper()

    def option_ENVID(self, value):
        # XXX: Check
        self.options['ENVID'] = value

    def option_RET(self, value):
        # XXX: Check
        self.options['RET'] = value.upper()

    def option_SIZE(self, value):
        try:
            self.options['SIZE'] = int(value)
        except ValueError as e:
            raise SmtpErrorException(501, '5.5.4', 'Bad message size syntax') from e


class SmtpRcptTo(SmtpAddr):
    __slots__ = ()

    def __init__(self, addr, options):
        super(SmtpRcptTo, self).__init__(addr, options)
        # XXX: Handle bare postmaster
        if '@' not in addr:
            raise SmtpErrorException(501, '5.1.3', 'Bad recipient address syntax')

    def option_NOTIFY(self, value):
        # XXX: Check
        self.options['NOTIFY'] = value.upper()

    def option_ORCPT(self, value):
        # XXX: Check
        self.options['ORCPT'] = value


class SmtpException(Exception):
    def __init__(self, encode, msg, code=None):
        super(SmtpException, self).__init__(msg)
        if not code:
            code = encode[0] == '4' and 450 or 550
        self.code, self.encode, self.msg = code, encode, msg


class SmtpErrorException(SmtpException):
    def __init__(self, code, encode, msg):
        super(SmtpErrorException, self).__init__(encode, msg, code)


class SmtpErrorLogException(SmtpErrorException):
    pass


class SmtpExitException(SmtpException):
    exits = {
        os.EX_SOFTWARE: ('5.3.0', 'Internal software error'),
        os.EX_OSERR: ('4.3.0', 'System resource problem'),
        os.EX_TEMPFAIL: ('4.3.0', 'Temporary failure'),
    }

    def __init__(self, exitcode):
        c = self.exits.get(exitcode) or ('5.3.0', 'Unknown error: {:d}'.format(exitcode))
        super(SmtpExitException, self).__init__(*c)


class SmtpTimeoutException(SmtpException):
    def __init__(self):
        super(SmtpTimeoutException, self).__init__('5.3.0', 'Command time limit exceeded')


def smtp_format_datetime(dt):
    timetuple = dt.timetuple()
    return '%02d %s %04d %02d:%02d:%02d -0000' % (
        timetuple[2],
        ('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
         'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec')[timetuple[1] - 1],
        timetuple[0], timetuple[3], timetuple[4], timetuple[5])

