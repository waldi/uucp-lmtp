# subprocess - Subprocesses with accessible I/O streams
#
# For more information about this module, see PEP 324.
#
# Copyright (c) 2003-2005 by Peter Astrand <astrand@lysator.liu.se>
#
# Licensed to PSF under a Contributor Agreement.
# See http://www.python.org/2.4/license for licensing details.

import os
import subprocess as _base
import time

from subprocess import Popen, PIPE, STDOUT, \
        call, check_call, check_output,\
        getstatusoutput, getoutput

try:
    from subprocess import TimeoutExpired

except ImportError:
    try:
        from time import monotonic as _time
    except ImportError:
        from time import time as _time


    class TimeoutExpired(Exception):
        """This exception is raised when the timeout expires while waiting for a
        child process.
        """
        def __init__(self, cmd, timeout, output=None):
            self.cmd = cmd
            self.timeout = timeout
            self.output = output

        def __str__(self):
            return ("Command '%s' timed out after %s seconds" %
                    (self.cmd, self.timeout))


    class Popen(_base.Popen):
        def __init__(self, args, *p, **kw):
            super(Popen, self).__init__(args, *p, **kw)
            self.args = args

        def _remaining_time(self, endtime):
            """Convenience for _communicate when computing timeouts."""
            if endtime is None:
                return None
            else:
                return endtime - _time()

        if _base.mswindows:
            raise NotImplementedError

        else:
            @staticmethod
            def _eintr_retry_call(func, *args):
                while True:
                    try:
                        return func(*args)
                    except (OSError, IOError) as e:
                        if e.errno == errno.EINTR:
                            continue
                        raise

            def _try_wait(self, wait_flags):
                try:
                    (pid, sts) = self._eintr_retry_call(os.waitpid, self.pid, wait_flags)
                except OSError as e:
                    if e.errno != errno.ECHILD:
                        raise
                    # This happens if SIGCLD is set to be ignored or waiting
                    # for child processes has otherwise been disabled for our
                    # process.  This child is dead, we can't get the status.
                    pid = self.pid
                    sts = 0
                return (pid, sts)


            def wait(self, timeout=None, endtime=None):
                """Wait for child process to terminate.  Returns returncode
                attribute."""
                if self.returncode is not None:
                    return self.returncode

                # endtime is preferred to timeout.  timeout is only used for
                # printing.
                if endtime is not None or timeout is not None:
                    if endtime is None:
                        endtime = _time() + timeout
                    elif timeout is None:
                        timeout = self._remaining_time(endtime)

                if endtime is not None:
                    # Enter a busy loop if we have a timeout.  This busy loop was
                    # cribbed from Lib/threading.py in Thread.wait() at r71065.
                    delay = 0.0005 # 500 us -> initial delay of 1 ms
                    while True:
                        (pid, sts) = self._try_wait(os.WNOHANG)
                        assert pid == self.pid or pid == 0
                        if pid == self.pid:
                            self._handle_exitstatus(sts)
                            break
                        remaining = self._remaining_time(endtime)
                        if remaining <= 0:
                            raise TimeoutExpired(self.args, timeout)
                        delay = min(delay * 2, remaining, .05)
                        time.sleep(delay)
                elif self.returncode is None:
                    (pid, sts) = self._try_wait(0)
                    self._handle_exitstatus(sts)
                return self.returncode

