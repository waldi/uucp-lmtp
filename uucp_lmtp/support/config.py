import os

from configparser import ConfigParser

def read_config(name=None):
    ret = ConfigParser()
    if name:
        ret.read_file(open(name))
    return ret
