"""
@copyright: 2012 Bastian Blank <waldi@debian.org>
@license: GNU GPL-3
"""
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from logging import root, INFO, Formatter, StreamHandler
from logging.handlers import SysLogHandler

def basic_config(name, level=INFO, use_std=True, use_syslog=True):
    if use_std:
        h = StreamHandler()
        f = Formatter("%(levelname)s:%(name)s: %(message)s")
        h.setFormatter(f)
        root.addHandler(h)
    if use_syslog:
        h = SysLogHandler(address='/dev/log', facility='mail')
        f = Formatter("{}[%(process)d]: %(message)s".format(name))
        h.setFormatter(f)
        root.addHandler(h)
    root.setLevel(level)
